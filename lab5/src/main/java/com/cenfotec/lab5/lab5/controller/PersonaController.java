package com.cenfotec.lab5.lab5.controller;

import com.cenfotec.lab5.lab5.domain.Persona;
import com.cenfotec.lab5.lab5.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

@Controller
public class PersonaController {

    @Autowired
    PersonaService personaService;

    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("persona", personaService.getAll());
        return "index";
    }

    @RequestMapping(value = "/agregarEntrada", method = RequestMethod.GET)
    public String navegarPaginaInsertar(Model model){
        model.addAttribute(new Persona());
        return "agregarEntrada";
    }

    @RequestMapping(value = "/agregarEntrada", method = RequestMethod.POST)
    public String accionPaginaInsertar(Persona persona, BindingResult result, Model model){
        persona.setNacimiento(Date.from(Instant.now()));
        personaService.savePersona(persona);

        return "exito";
    }

    @RequestMapping(value = "/editar/{id}")
    public String irAEditar(Model model, @PathVariable int id) {
        Optional<Persona> personaToEdit = personaService.getById(id);

        if (personaToEdit.isPresent()){
            model.addAttribute("personaToEdit", personaToEdit);
            return "editForm";
        } else {
            return "notFound";
        }
    }

    @RequestMapping(value = "/editar/{id}", method = RequestMethod.POST)
    public String guardarCambios(Persona persona, BindingResult result,Model model,
                                 @PathVariable int id) {
        persona.setNacimiento(Date.from(Instant.now())); // esto normalmente
        //podria ir en el service.
        personaService.updatePersona(persona);
        return "exito";
    }


    @RequestMapping(value = "/borrar/{id}")
    public String borrar(Model model, @PathVariable int id){
        personaService.deletePersona(id);

        return "exito";
    }
}
