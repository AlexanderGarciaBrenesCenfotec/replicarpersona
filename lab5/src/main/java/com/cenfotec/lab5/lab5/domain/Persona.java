package com.cenfotec.lab5.lab5.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String direccion;
    private Date nacimiento;

    @Transient
    private SimpleDateFormat format = new SimpleDateFormat("yyyy/mm/dd");

    public Persona(String nombre, String apellidoUno, String apellidoDos, String direccion, String fecha) throws ParseException {
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.direccion = direccion;
        this.nacimiento = format.parse(fecha);
    }

    public Persona() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }
    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }
    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getNacimiento() {
        return nacimiento;
    }
    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getCreatedAsShort() {
        return format.format(nacimiento);
    }

    public String toString() {
        StringBuilder value = new StringBuilder("JournalEntry(");
        value.append("Nombre: ");
        value.append(nombre);
        value.append(", Apellido Uno: ");
        value.append(apellidoUno);
        value.append(", Apellido Dos: ");
        value.append(apellidoDos);
        value.append(", Dirección: ");
        value.append(direccion);
        value.append(", Fecha de nacimiento: ");
        value.append(getCreatedAsShort());
        value.append(")");
        return value.toString();
    }

}